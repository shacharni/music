<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{


    protected $fillable = ['title','total_time','quantity_of_songs','status',
        
];
    
    public function user(){
        return $this-> belongsTo('App\User');
    }
   
}

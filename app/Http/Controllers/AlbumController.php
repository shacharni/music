<?php

namespace App\Http\Controllers;
use\Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Album;
use App\User;

class AlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $albums=Album::all();
        return view("albums.index",['albums'=>$albums]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('artist')) {
            abort(403,"Sorry you are not allowed to create todos..");
             } 
  

        return view("albums.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id=Auth::id();
        $album=new Album();
        $album->title=$request->title;
        $album->total_time=$request->total_time;
        $album->quantity_of_songs=$request->quantity_of_songs;
        $album->artist_id=$id;
        $album->save();
        return redirect('albums');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('artist')) {
            abort(403,"Are you a hacker or what?");
              }

       
        $album=Album::find($id);
        return view('albums.edit',['album'=>$album]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $album = Album::findOrFail($id);
             
       if (Gate::denies('artist')) {
        if ($request->has('title'))
          abort(403,"You are not allowed to edit albums..");
       }  
          if (!$album->artist_id==Auth::id()) return(redirect('albums'));
          $album->update($request->except(['_token']));
          if($request->ajax()){
              return Response::json(array('result'=>'success','status'=>$request->status),200);
          }
          $album->update($request->all());
          return redirect ('albums');
  
        
  
       
    }
          
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('artist')) {
            abort(403,"Are you a hacker or what?");
        }

        $album=Album::find($id);
        if (!$album->artist_id==Auth::id()) return(redirect('albums'));
        $album->delete();
        return redirect ('albums');

    }
    public function favorite($id,$favorite){

        if (Gate::denies('customer')) {
            abort(403,"Are you a hacker or what?");
              }
      $album=Album::find($id);
      $album->favorite=$favorite;
      $album->save();
      return redirect ('albums');
}
//public function show_favorite(){
   
 //  $albums=Album::all();
   //$album=$albums->find($favorite>=1);
   //$albums = $albums->diff(Album::whereIn('favorite', [!0]));
  
    //return view("albums.index",['albums'=>$albums]);
//}



}

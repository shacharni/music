@extends('layouts.app')

@section('content')
<h1>Update a album</h1>
<form method='post' action="{{action('AlbumController@update',$album->id)}}">
    @csrf
    @method('PATCH')

    <div class="form-group">
    <label for ="edit"> album to update</label>
    <div>
        <label for ="title"> new title</label>
        <input type="text" class= "form-control" name="title" value="{{$album->title}}">
        </div>
        <div>
        <label for ="total_time"> new total time</label>
        <input type="text" class= "form-control" name="total_time" value="{{$album->total_time}}">
        </div>
        <div>
        <label for ="quantity_of_songs"> new quantity_of_songs</label>
        <input type="text" class= "form-control" name="quantity_of_songs" value="{{$album->quantity_of_songs}}">
        </div>
    </div>
    
    <div class = "form-group">
        <input type="submit" class="form-controll" name="submit" value="update">
    </div>
</form>


@endsection

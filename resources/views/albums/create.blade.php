@extends('layouts.app')

@section('content')
<h1>Create a new album</h1>
<form method='post' action="{{action('AlbumController@store')}}">
    {{csrf_field()}}

    <div class="form-group">
    <h2>Add a album</h2>
        <label for ="title">title </label>
        <input type="text" class= "form-control" name="title">
        <label for ="title">total time </label>
        <input type="text" class= "form-control" name="total_time">
        <label for ="title">number of songs </label>
        <input type="text" class= "form-control" name="quantity_of_songs">
        </div>
    <div class = "form-group">
        <input type="submit" class="form-control" name="submit" value="save">
    </div>
</form>
@endsection
@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html>
<h1> this is a album list</h1>
    
    <body>
  
   <ul> @foreach($albums as $album)
   
  
   @if ($album->status)
   @can('artist')   <input type = 'checkbox' id ="{{$album->id}}" checked>
       @else
           <input type = 'checkbox' id ="{{$album->id}}">
       @endif
       @endcan
       @cannot('artist') <a href="{{route('albums.favorite', ['id' =>$album->id,'favorite'=>$album->favorite+1])}}">
insert in </a>@endcannot
@if (($album->favorite)>3)
<b> album name: </b><i>{{$album->title}}</i> @can('artist') <a href="{{route('albums.edit',$album->id)}}"> edit</a>@endcan
    <li> <u> total time:</u>{{$album->total_time}} </li>
    <li><u>number of songs:</u> {{$album->quantity_of_songs}} </li>
    @else
 <b> album name: </b>{{$album->title}} @can('artist') <a href="{{route('albums.edit',$album->id)}}"> edit</a>@endcan
    <li> <u> total time:</u>{{$album->total_time}} </li>
    <li><u>number of songs:</u> {{$album->quantity_of_songs}} </li>
    @endif
    @can('artist') <form method='post' action="{{action('AlbumController@destroy',$album->id)}}"> 
    @csrf
    @method('DELETE')

    <div class = "form-group">
        <input type="submit" class="btn btn-link" name="submit" value="delete">
    </div>
</form> @endcan
@endforeach
          </ul>
          @can('artist')   <a href="{{route('albums.create')}}">Create a new album</a>@endcan

          <script>
      $(document).ready(function(){
          $(":checkbox").click(function(event){
              console.log(event.target.id)
               $.ajax({
                   url: "{{url('albums')}}"+'/' + event.target.id,
                   dataType: 'json',
                   type: 'put' ,
                   contentType:'application/json',
                   data:JSON.stringify({'status':event.target.checked, _token:"{{csrf_token()}}"}),
                   processData:false,
                  success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script>  
</body>
</html>
@endsection